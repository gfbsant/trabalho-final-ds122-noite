<?php
  require "authenticate.php";
  require_once "db_credentials.php";
  include "sanitize.php";

  $conn = mysqli_connect($servername, $username, $db_password, $dbname);
    if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
    }

    $form_name = $form_comment = $form_artigoID = "";
    $msg = "";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if(isset($_POST['form_comment']) && isset($_POST['form_artigoID']) && ($_POST['form_comment'] <> "")){
        $form_name = $_SESSION['user_name'];
        $form_comment = $_POST['form_comment'];
        $form_artigoID = $_POST['form_artigoID'];

        $name = mysqli_real_escape_string($conn, $form_name);
        $comment = mysqli_real_escape_string($conn, $form_comment);
        $artigoID = mysqli_real_escape_string($conn, $form_artigoID);
        $name = sanitize($name);
        $comment = sanitize($comment);
        $artigoID = sanitize($artigoID);

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Y-m-d H:i:s');


        $sql = "INSERT INTO $table_comments (nome, comentario, artigoID, criacao, atualizacao)
        VALUES ('$name', '$comment', '$artigoID', '$date', '$date')";



        if (!mysqli_query($conn, $sql)) {
          die("Error: " . $sql . "<br>" . mysqli_error($conn));
        }
        else {
          $form_name = $form_comment = $form_artigoID = "";
          $msg = "Comentário salvo com sucesso!". $artigoID;
        }
      }
      elseif(isset($_POST["novo-comentario"]) && isset($_POST["id"]) && isset($_POST["artigoID"])){

        $novo_comentario = $_POST["novo-comentario"];
        $novo_comentario = sanitize($novo_comentario);
        $id = $_POST["id"];
        $id = sanitize($id);
        $artigoID = $_POST["artigoID"];
        $artigoID = sanitize($artigoID);


        $sql = "UPDATE $table_comments
            SET comentario='". mysqli_real_escape_string($conn, $novo_comentario) .
            "', atualizacao='" . date('Y-m-d H:i:s') . "' WHERE id=" . mysqli_real_escape_string($conn, $id);

        if(!mysqli_query($conn,$sql)){
          die("Problemas para executar ação no BD!<br>".
            mysqli_error($conn));
        } else {
          header("Location: " . dirname($_SERVER['HTTP_HOST']) . "/index.php#form-anchor" . $artigoID . "comment");
          exit();
        }
      }
    }
    elseif ($_SERVER['REQUEST_METHOD'] == 'GET') {
      if (isset($_GET["acao"]) && isset($_GET["id"])) {
        $sql = "";
        $id = $_GET['id'];
        $id = mysqli_real_escape_string($conn, $id);
        $id = sanitize($id);
        if($_GET["acao"] == "remove"){
          $sql = "DELETE FROM $table_comments
              WHERE id=" . $id;
        }
        if($_GET["acao"] == "removetxt"){
          $sql = "DELETE FROM $table_articles
              WHERE id=" . $id;
        }
        if ($sql != "") {
          if(!mysqli_query($conn,$sql)){
            die("Problemas para executar ação no BD!<br>".
              mysqli_error($conn));
          }
        }
      }
    }


    $sql = "SELECT * FROM $table_comments";
    $comments = mysqli_query($conn, $sql);

    $name = "SELECT nome FROM $table_comments WHERE ";

    if (!$comments) {
      die("Error: " . $sql . "<br>" . mysqli_error($conn));
    }

    $sql = "SELECT * FROM $table_articles";
    $textos = mysqli_query($conn, $sql);

    if (!$textos) {
      die("Error: " . $sql . "<br>" . mysqli_error($conn));
    }

    mysqli_close($conn);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript">
      function hideComments(x) {
        var a = "comments_";
        var b = "hideButton_";
        var y = a + x;
        var z = b + x;
        var cmts = document.getElementById(y);
        var btna = document.getElementById(z);
        if (cmts.style.display === 'none') {
          cmts.style.display = 'block';
          btna.innerHTML = 'OCULTAR COMENTARIOS';
        } else {
          cmts.style.display = 'none';
          btna.innerHTML = 'MOSTRAR COMENTARIOS';
        }  
      }
    </script> 
		<link rel="stylesheet" href="stylesheet.css">
    <title>Blog Inter 2</title>
	</head>
	<body>
     <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Blog Inter 2</a>
        </div>
        <ul class="nav navbar-nav">
          <li class="active"><a href="index.php">Home</a></li>
          <?php if ($login): ?>
            <li><a href="logout.php">Logout</a></li>
          <?php else: ?>
            <li><a href="login.php">Login</a></li>
            <li><a href="register.php">Registrar-se</a></li>
          <?php endif; ?>
        </ul>
      </div>
    </nav>
    <?php if(isset($_SESSION['user_name'])): ?>
        <div class="bv">
          <h4>
            <?php echo "Bem-vindo, ".$_SESSION['user_name']."!";?>
          </h4>
        </div>
    <?php endif; ?>
    <div class="container">
      <div id=botaofinal>
        <?php if($login && ($_SESSION['user_name'] == "admin")): ?>
            <a href="cria_texto.php" class="btn btn-primary btn-block" role="button">Criar novo artigo!</a>
          <?php endif; ?>
        </div>
    </div><hr>

        <div id="texts" class="text-center">
          <div class="jumbotron">
          <h1>Postagens<h1>
        </div>
        <?php if (mysqli_num_rows($textos) > 0): ?>
          <?php while($texto = mysqli_fetch_assoc($textos)): ?>
      <div class="container">
            <hr>
            <div class="container" id="texto_<?= $texto['id'] ?>">
              <a name="form-anchor<?php echo $texto['id']; ?>texto"></a>
              <h1 class="titulo"><?= $texto['titulo'] ?></h1>

              <?php if ($texto['criacao'] == $texto['atualizacao']): ?>
                    <h4></strong> feito as <i><?= $texto['criacao'] ?></i></h4>
                      <p><?= $texto['comentario'] ?></p>

                    <?php else: ?>
                      <h4></strong> feito as <i><?= $texto['criacao'] ?></i> atualizado em <i><?= $texto['atualizacao'] ?></i></h4>
                      <p><?= $texto['comentario'] ?></p>
                    <?php endif; ?>   
              <p><?= $texto['texto'] ?></p>
              
              <?php if($login && ($_SESSION['user_name'] == "admin" || $texto["autor"] == $_SESSION['user_name'])): ?>
                <a class="btn btn-primary btn-lg" href="<?php echo $_SERVER["PHP_SELF"] . "?id=" . $texto["id"] . "&" . "acao=removetxt" ?>">Remover</a>
                <a class="btn btn-secondary btn-lg" href="edita_texto.php?id=<?php echo $texto["id"]; ?>">Editar</a>
              <?php endif; ?>
              </div>  

              <hr>
              <div class="mostra-comment">
                <a id="hideButton_<?= $texto['id'] ?>" class="btn btn-primary"
                onclick="hideComments(<?= $texto['id'] ?>)">MOSTRAR COMENTARIOS</a>
               </div>
              <br><br>
              <div style="display:none;" class="container" id="comments_<?= $texto['id'] ?>">
                <a name="form-anchor<?php echo $texto['id']; ?>comment"></a>
                <h2>Comentários</h2>

                <?php if (!empty($msg) && (str_contains($msg, $texto['id']))): ?>
                  <?= str_replace($texto['id'], "", $msg); ?>
                <?php endif; ?>

                <?php
                  $conn = mysqli_connect($servername, $username, $db_password, $dbname);
                  if (!$conn) {
                    die("Connection failed: " . mysqli_connect_error());
                  }

                  $textoID = $texto['id'];

                  $sql = "SELECT * FROM $table_comments WHERE artigoID = $textoID";

                  $commentsID = mysqli_query($conn, $sql);

                  if (!$commentsID) {
                    die("Error: " . $sql . "<br>" . mysqli_error($conn));
                  }

                  mysqli_close($conn);
                ?>

                <?php if (mysqli_num_rows($commentsID) > 0): ?>
                  <?php while($comment = mysqli_fetch_assoc($commentsID)): ?>
                    <div class="comment" id="comment_<?= $comment['id'] ?>">

                    <?php if ($comment['criacao'] == $comment['atualizacao']): ?>
                    <h4>De: <strong><?= $comment['nome'] ?></strong> feito as <i><?= $comment['criacao'] ?></i></h4>
                      <p><?= $comment['comentario'] ?></p>

                    <?php else: ?>
                      <h4>De: <strong><?= $comment['nome'] ?></strong> feito as <i><?= $comment['criacao'] ?></i> atualizado em <i><?= $comment['atualizacao'] ?></i></h4>
                      <p><?= $comment['comentario'] ?></p>
                    <?php endif; ?>        

                      <?php if($login): ?>
                        <a class="btn btn-danger" href="<?php echo $_SERVER["PHP_SELF"] . "?id=" . $comment["id"] . "&" . "acao=remove" ?>">Remover</a>
                        <a class="btn btn-info" href="edita_comentario.php?id=<?php echo $comment["id"]; ?>">Editar</a>
                      <?php endif; ?>
                    </div>
                  <?php endwhile; ?>
                <?php else: ?>
                  Nenhum comentário enviado.
                <?php endif; ?>

                <?php if($login): ?>
                <hr>
                <h3>Novo comentário</h3>
                <form method="post" action="<?= sanitize($_SERVER['PHP_SELF']) ?>#form-anchor<?php echo $texto['id']; ?>comment">
                  <input type="hidden" name="form_artigoID" value="<?php echo $texto["id"] ?>">
                  <div class="form-group">
                    <label class="lab" for="form_comment">Comentário:</label><br>
                    <textarea name="form_comment" rows="8" cols="80" placeholder="Seu comentário"><?= $form_comment ?></textarea><br>
                  </div>
                  <input class="btn btn-default" type="submit" name="submit" value="Enviar"><br><br>
                </form>
                <?php endif; ?>
              </div>
            </div>
          <?php endwhile; ?>
        <?php else: ?>
          Nenhum artigo publicado.
        <?php endif; ?>
      </div>
      </div>
	</body>
</html>

