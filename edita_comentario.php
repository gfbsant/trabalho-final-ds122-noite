<?php
require 'db_credentials.php';
include 'sanitize.php';

$conn = mysqli_connect($servername,$username,$db_password,$dbname);
if (!$conn) {
  die("Problemas ao conectar com o BD!<br>".
       mysqli_connect_error());
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if (isset($_GET["id"])) {

    $id = $_GET['id'];
    $id = mysqli_real_escape_string($conn, $id);
    $id = sanitize($id);

    $sql = "SELECT id,comentario,artigoID FROM $table_comments WHERE id = ". $id;

    if(!($comment = mysqli_query($conn,$sql))){
      die("Problemas para carregar comentários do BD!<br>".
           mysqli_error($conn));
    }
  }
}
mysqli_close($conn);
if (mysqli_num_rows($comment) != 1) {
    die("Id de comentário incorreto.");
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Editar Comentário</title>
</head>
<body>
<div class="container mt-5">
<div class="card p-4">
<form action="index.php" method="POST">
        <div class="mb-3">
          <?php $comment = mysqli_fetch_assoc($comment); ?>
          <input type="hidden" name="artigoID" value="<?php echo $comment["artigoID"] ?>">
          <input type="hidden" name="id" value="<?php echo $comment["id"] ?>">
          <h1>Editar Comentário</h1><br>
          <div class="mb-3">
            <textarea required class="form-control" name="novo-comentario" rows="8" cols="80" placeholder="Texto do comentario"><?php echo $comment["comentario"]; ?></textarea><br>
          </div>
          <button class="btn btn-primary me-2" type="submit" name="submit"><i class="fas fa-save me-2"></i>Enviar</button>
            <a class="btn btn-secondary" href="index.php"><i class="fas fa-arrow-left me-2"></i>Voltar</a>
        </div>
      </form>
</div>
</div>
</body>
</html>