<?php
  session_start();

  session_destroy();

  header("Location: " . dirname($_SERVER['HTTP_HOST']) . "/index.php");

?>
