# Trabalho Prático

## Integrantes:
```
Gabriel Felipe Batista dos Santos - GRR20221101
Eduardo Vinícius Frohlich         - GRR20221126
Gustavo Iassyl Nielsen Alves      - GRR20221083
Gustavo Jahnz                     - GRR20221117
Jorge Victor Bencke Gastaldi      - GRR20221123


```

## Procedimentos iniciais

- Antes de executar o código é necessário primeiro ter o php-mysqli instalado em sua máquina, para que o banco de dados possa ser criado.
  Para instalar basta executar o comando `sudo apt install php-mysqli`.

- Após isso, conectar o servidor php na pasta dos arquivos com
  `php -S localhost:8080 `.
- Primeiro deve-se alterar os dados de db_credentials.php com os dados respectivos a sua máquina:
  // exemplo //
  $username = "root"; // usuário do seu bd
$db_password = "12345678"; // senha para seu bd

Em seguida deve-se acessar no website a página alterada: localhost:8080/db_credentials.php.

- Acessar no website a página create_db_tables para a criação do banco de dados, em caso de sucesso na criação deverá aparecer na tela:

```
Database created successfully
Database changed successfully
Table 'Users' created successfully
Table "Comentarios" created successfully
Table 'Artigos' created successfully
User admin created successfully
```

## Descrição geral do Blog Inter 2
 - O Blog Inter 2 tem como objetivo o cadastro de usuários que podem vir a inserir ou atualizar _posts_ já criados. Esses _posts_ possuem datas de criação e de atualiazação, podendo possuir zero ou mais comentários.

## Arquivos


### authenticate.php
-  Este arquivo serve para verificar se o usuário está autenticado em uma sessão e permitir que ele acesse recursos protegidos apenas para **usuários autenticados.**

### create_db_tables.php 
- Este Arquivo é o responsável por criar uma conexão com o banco de dados MySQL e executar algumas consultas para criar o banco de dados, as tabelas e inserir um usuário administrador.

### cria_texto.php
- Este arquivo serve para criar um formulário HTML para que os usuários possam enviar um artigo para ser armazenado no banco de dados MySQL. 

- A primeira parte do código estabelece uma conexão com o banco de dados usando as credenciais fornecidas no arquivo `db_credentials.php` e inicia uma sessão. 

- O restante do código seria responsável por criar um formulário HTML com campos para o título e o texto do artigo.

### db_credentials.php
- Este arquivo PHP define variáveis que armazenam informações de conexão com o banco de dados.

### db_functions.php
- Este arquivo PHP apresenta duas funções que facilitam a conexão e a desconexão com o banco de dados 

1.   **`connect_db()`:**  Estabelece a conexão com o banco de dados utilizando as informações contidas no arquivo.
```php
function connect_db(){
  global $servername, $username, $db_password, $dbname;
  $conn = mysqli_connect($servername, $username, $db_password, $dbname);

  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }

  return($conn);
}

```
2.  **`disconnect_db($conn)`:** Recebe o objeto de conexão mysqli como parâmetro e fecha a conexão com o banco de dados.
```php
function disconnect_db($conn){
  mysqli_close($conn);
}

```
### edita_comentario.php
- Este arquivo faz a busca de um comentário específico dentro do banco de dados com base no que lhe foi fornecido por meio do **método HTTP GET**. Em seguida, ele exibe um formulário para editar esse comentário.

### edita_texto.php
- Este arquivo PHP faz a edição de um artigo em um banco de dados MySQL. 

- Ele recebe um id de artigo por meio do **método GET**, busca esse artigo no banco de dados e exibe um formulário para editar o texto do artigo. 

- Quando o usuário submete o formulário, o novo texto é enviado para o arquivo `cria_texto.php` por meio do **método POST** para ser atualizado no banco de dados.

### index.php
- Este arquivo cria um sistema de comentários para a página web o **Blog Inter 2**.

- O código estabelece uma conexão com o banco de dados MySQL usando as credenciais armazenadas no arquivo `db_credentials.php`, e usa as funções `sanitize.php` e `authenticate.php`. 

- O código define uma série de ações que o sistema pode executar em resposta a solicitações **HTTP GET e POST**, como inserir novos comentários, atualizar ou remover comentários existentes.

- Além disso, o código exibe todos os comentários e artigos armazenados no banco de dados em uma página HTML usando **Bootstrap.**

- O JavaScript é usado para permitir que os usuários expandam ou ocultem os comentários associados a cada artigo.

### login.php 
- Este arquivo implementa uma página de login. Ele recebe um email e uma senha do usuário, verifica se as informações são válidas consultando um banco de dados.

- Se for bem-sucedido, cria uma sessão para o usuário e o redireciona para a página inicial. 


### logout.php 
- Este arquivo encerra a sessão do usuário e redireciona o navegador para a página inicial do site. 

- Ele faz isso usando as funções:
1. **`session_start():`** para iniciar a sessão;

2. **`session_destroy():`** para encerrá-la ;

3. **`header():`** para enviar um cabeçalho HTTP de redirecionamento para o navegador, apontando-o para a página inicial do site;
```php
<?php
  session_start();

  session_destroy();

  header("Location: " . dirname($_SERVER['HTTP_HOST']) . "/index.php");

?>

```
### register.php 
- Este arquivo serve para registrar um novo usuário no site Blog Inter 2. 

- Cria uma nova entrada no banco de dados por meio dos dados recbidos. O script primeiro verifica se os dados foram enviados via **método POST** e, em seguida, usa funções de limpeza e escape para evitar injeções de SQL. 

- Se a inserção for bem-sucedida, a sessão do usuário é 
criada e redirecionada para a página inicial do site.

- Se ocorrer um erro, uma mensagem de erro é exibida e o formulário é recarregado.

### sanitize.php
- Esse arquivo contém duas funções em PHP:`sanitize` e `sanitizetxt`. Sendo ambas responsáveis por limpar as entradas de texto dos formulários web criados antes de serem processadas pelo código da aplicação.

```php
<?php
function sanitize($txt){
    $txt = htmlspecialchars($txt);
    $txt = trim($txt);
    $txt = stripcslashes($txt);
    return $txt;
}
function sanitizetxt($txt){
    $txt = trim($txt);
    $txt = stripcslashes($txt);
    return $txt;
}
?>

```

### tempCodeRunnerFile.php
- Este arquivo realiza a conexão com o banco de dados MySQL utilizando as credenciais contidas no arquivo `db_credentials.php`.

- Em seguida, ele cria um novo banco de dados e três tabelas: `table_users`, `table_comments` e `table_articles`, usando as variáveis definidas no arquivo `db_credentials.php`. 

- Após a criação dessas tabelas, um novo usuário administrador é inserido na tabela `table_users`.

- Finalizando com o fechamento da conexão com o banco de dados.

### stylesheet.css
- Arquivo CSS que define estilos visuais para a página da web Blog Inter 2.
