<?php
  require "db_functions.php";
  require "sanitize.php";
  require "authenticate.php";

  session_start();

  // Redirect to homepage if user is already logged in
  if (isset($_SESSION["user_id"])) {
    header("Location: index.php");
    exit();
  }

  $error = false;
  $email = "";

  if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if (isset($_POST["email"]) && isset($_POST["password"])) {
      $conn = connect_db();
      $email = sanitize($_POST["email"]);
      $password = sanitize($_POST["password"]);
      $password = md5($password);

      $sql = "SELECT id, name, email, password FROM $table_users WHERE email = '$email'";
      $result = mysqli_query($conn, $sql);

      if ($result) {
        if (mysqli_num_rows($result) > 0) {
          $user = mysqli_fetch_assoc($result);

          if ($user["password"] === $password) {
            $_SESSION["user_id"] = $user["id"];
            $_SESSION["user_name"] = $user["name"];
            $_SESSION["user_email"] = $user["email"];
            $_SESSION["login_success"] = true;
            header("Location: index.php");
            exit();
          } else {
            $error = true;
          }
        } else {
          $error = true;
        }
      } else {
        $error = true;
      }
    } else {
      $error = true;
    }
  }
  
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
    <style>
      form {
        margin-top: 20px;
      }
      .form-group {
        margin-bottom: 20px;
      }
      .form-group label {
        font-weight: bold;
        display: block;
        margin-bottom: 5px;
      }
      .form-group input {
        display: block;
        width: 100%;
        padding: 8px;
        border-radius: 4px;
        border: 1px solid #ccc;
      }
      .form-group input:focus {
        outline: none;
        border-color: #343a40;
        box-shadow: 0px 0px 5px #343a40;
      }
      .form-group button {
        display: block;
        width: 100%;
        padding: 10px;
        background-color: #343a40;
        color: #fff;
        border: none;
        border-radius: 4px;
        cursor: pointer;
      }
      .form-group .message {
        margin-top: 10px;
        font-size: 14px;
        color: red;
      }

      .card-body {
        padding: 0;
      }

      .card {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        max-width: 400px;
        height: 500px;
        padding: 30px 20px;
        background-color: #fff;
        border-radius: 5px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.3);
      }
      .card-header {
        text-align: center;
        font-size: 2rem;
        margin-bottom: 10px;
      }


</style>
  </head>
  <body>
    <div class="container">
      <div class="card">
        <div class="card-header">
          <h1>Login</h1>
        </div>
        <div class="card-body">
          <?php if ($error): ?>
            <div class="alert alert-danger">Invalid email or password</div>
          <?php endif; ?>
          <form method="post">
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" name="email" id="email" value="<?php echo $email; ?>" required>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" name="password" id="password" required>
            </div>
            <div class="form-group">
              <button type="submit">Login</button>
            </div>
            <div class="d-grid gap-2 mt-2">
              <a class="btn btn-secondary" href="index.php">Voltar</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>