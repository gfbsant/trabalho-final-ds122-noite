<?php
  require_once "db_credentials.php";
  include "sanitize.php";

  session_start();
  $autor = $_SESSION['user_name'];

  $conn = mysqli_connect($servername, $username, $db_password, $dbname);
  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }

  $form_titulo = $form_texto = "";
  $msg_texto = "";

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['form_titulo']) && isset($_POST['form_texto'])){
      $form_titulo = $_POST['form_titulo'];
      $form_texto = $_POST['form_texto'];
      $form_texto = nl2br($form_texto);

      $titulo = mysqli_real_escape_string($conn, $form_titulo);
      $texto = mysqli_real_escape_string($conn, $form_texto);

      $titulo = sanitizetxt($titulo);
      $texto = sanitizetxt($texto);

      date_default_timezone_set('America/Sao_Paulo');
      $date = date('Y-m-d H:i:s');


      $sql = "INSERT INTO $table_articles (titulo, texto, autor, criacao, atualizacao)
        VALUES ('$titulo', '$texto', '$autor', '$date', '$date')";

      if (!mysqli_query($conn, $sql)) {
        die("Error: " . $sql . "<br>" . mysqli_error($conn));
      }
      else {
        $msg_texto = "Artigo salvo com sucesso!";
        header("Location: " . dirname($_SERVER['HTTP_HOST']) . "/index.php");
        exit();
      }
    }
    elseif(isset($_POST["novo-texto"]) && isset($_POST["id"])){
      $novo_texto = $_POST["novo-texto"];
      $novo_texto = nl2br($novo_texto);
      $novo_texto = sanitizetxt($novo_texto);
      $id = $_POST["id"];
      $id = sanitize($id);

      $sql = "UPDATE $table_articles
          SET texto='". mysqli_real_escape_string($conn, $novo_texto) .
          "' WHERE id=" . mysqli_real_escape_string($conn, $id);

      if(!mysqli_query($conn,$sql)){
        die("Problemas para executar ação no BD!<br>".
          mysqli_error($conn));
      }
      else {
          $msg = "Artigo atualizado com sucesso!";
          header("Location: " . dirname($_SERVER['HTTP_HOST']) . "/index.php");
          exit();
      }
    }
  }

  mysqli_close($conn);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Criar Texto</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Voltar</a>
      </div>
    </nav>

    <div class="container mt-5">
      <div class="card p-4">
        <h1 class="mb-4">Criar Texto</h1> <form action="<?= sanitize($_SERVER['PHP_SELF']) ?>" method="post">
          <div class="mb-3">
            <label for="form_titulo" class="form-label">Título</label>
            <input class="form-control" type="text" name="form_titulo" value="<?= $form_titulo ?>" placeholder="Título do artigo">
          </div>
          <div class="mb-3">
            <label for="form_texto" class="form-label">Texto</label>
            <textarea class="form-control" name="form_texto" rows="10" placeholder="Texto do artigo"><?= $form_texto ?></textarea>
          </div>
          <button class="btn btn-primary me-2" type="submit" name="submit"><i class="fas fa-save me-2"></i>Enviar</button>
          <a class="btn btn-secondary" href="index.php"><i class="fas fa-arrow-left me-2"></i>Voltar</a>
        </form>
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.3/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/js/bootstrap.min.js"></script>
  </body>
</html>
