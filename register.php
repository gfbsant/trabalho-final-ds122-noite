<?php
require "db_functions.php";
include "sanitize.php";

session_start();

$error = false;
$success = false;
$name = $email = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["confirm_password"])) {

    $conn = connect_db();

    $name = mysqli_real_escape_string($conn,$_POST["name"]);
    $email = mysqli_real_escape_string($conn,$_POST["email"]);
    $password = mysqli_real_escape_string($conn,$_POST["password"]);
    $confirm_password = mysqli_real_escape_string($conn,$_POST["confirm_password"]);
    $name = sanitize($name);
    $email = sanitize($email);
    $password = sanitize($password);
    $confirm_password = sanitize($confirm_password);

    if (filter_var($email, FILTER_VALIDATE_EMAIL)){
      if ($password == $confirm_password) {
        $password = md5($password);

        $sql = "INSERT INTO $table_users
                (name, email, password) VALUES
                ('$name', '$email', '$password');";

        
        if(mysqli_query($conn, $sql)){
        // Registro bem-sucedido, autenticando o usuário
        $user_id = mysqli_insert_id($conn);
        $_SESSION["user_id"] = $user_id;
        $_SESSION["user_name"] = $name;
        $_SESSION["user_email"] = $email;
        header("Location: index.php");
        exit();
        }

        else {
          $error_msg = mysqli_error($conn);
          $error = true;
        }
      }
      else {
        $error_msg = "Senha não confere com a confirmação.";
        $error = true;
      }
    } 
    else {
      $error_msg = "Formato de email inválido";
      $error = true;
    }  
  }
  else {
    $error_msg = "Por favor, preencha todos os dados.";
    $error = true;
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <title>Registro - Blog Inter 2</title>
  </head>
  <body>
    <div class="container mt-5">
      <h1 class="mb-4">Dados para registro de novo usuário</h1>

      <?php if ($success): ?>
        <div class="alert alert-success" role="alert">
          Usuário criado com sucesso!
        </div>
      <?php endif; ?>

      <?php if ($error): ?>
        <div class="alert alert-danger" role="alert">
          <?php echo $error_msg; ?>
        </div>
      <?php endif; ?>

      <form action="register.php" method="post">
        <div class="mb-3">
          <label for="name" class="form-label">Nome:</label>
          <input type="text" class="form-control" name="name" value="<?php echo $name; ?>" required>
        </div>
        <div class="mb-3">
          <label for="email" class="form-label">Email:</label>
          <input type="text" class="form-control" name="email" value="<?php echo $email; ?>" required>
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">Senha:</label>
          <input type="password" class="form-control" name="password" value="" required>
        </div>
        <div class="mb-3">
          <label for="confirm_password" class="form-label">Confirmação da Senha:</label>
          <input type="password" class="form-control" name="confirm_password" value="" required>
        </div>
        <div class="btn btn-primary mb-2">
          <button class="btn btn-primary btn-sm" type="submit" name="submit">Criar usuário</button>
        </div>
        <div class="btn btn-secondary mb-2">
          <a class="btn btn-secondary btn-sm" href="index.php">Voltar</a>
        </div>
      </form>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  </body>
</html>

