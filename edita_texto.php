<?php
require_once "db_credentials.php";
include "sanitize.php";

session_start();
$autor = $_SESSION['user_name'];

$conn = mysqli_connect($servername, $username, $db_password, $dbname);
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

$id = $_GET['id'];
$id = sanitize($id);

$sql = "SELECT * FROM $table_articles WHERE id = $id";
$result = mysqli_query($conn, $sql);

if(mysqli_num_rows($result) == 1) {
  $row = mysqli_fetch_assoc($result);
  $form_titulo = $row['titulo'];
  $form_texto = $row['texto'];
}
else {
  die("Artigo não encontrado!");
}

$msg_texto = "";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if(isset($_POST["novo-titulo"]) && isset($_POST["novo-texto"])){
    $novo_titulo = $_POST["novo-titulo"];
    $novo_texto = $_POST["novo-texto"];
    $novo_texto = nl2br($novo_texto);
    $novo_texto = sanitizetxt($novo_texto);

    $sql = "UPDATE $table_articles
        SET titulo='". mysqli_real_escape_string($conn, $novo_titulo) .
        "', texto='". mysqli_real_escape_string($conn, $novo_texto) .
        "', atualizacao='". date('Y-m-d H:i:s') .
        "' WHERE id=" . mysqli_real_escape_string($conn, $id);

    if(!mysqli_query($conn,$sql)){
      die("Problemas para executar ação no BD!<br>".
        mysqli_error($conn));
    }
    else {
        $msg_texto = "Artigo atualizado com sucesso!";
    }
  }
}

mysqli_close($conn);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Editar Texto</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
  <style>
    body {
      background-color: #f8f9fa;
    }
    .card {
      max-width: 800px;
      margin: 0 auto;
      background-color: #fff;
      border-radius: 5px;
      padding: 20px;
      margin-top: 20px;
    }
  </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php">Voltar</a>
  </div>
</nav>

<div class="container mt-5">
  <div class="card p-4">
    <h1 class="mb-4">Editar Texto</h1>
    <form id="editar-post-form" action="" method="post">
      <div class="mb-3">
        <label for="novo-titulo" class="form-label">Novo Título</label>
        <input type="text" class="form-control" id="novo-titulo" name="novo-titulo" value="<?php echo $form_titulo; ?>">
      </div>
      <div class="mb-3">
        <label for="novo-texto" class="form-label">Novo Texto</label>
        <textarea class="form-control" id="novo-texto" name="novo-texto" rows="10"><?php echo $form_texto; ?></textarea>
      </div>
      <div class="mb-3">
        <input type="submit" value="Atualizar" class="btn btn-primary">
      </div>
    </form>
    <?php if($msg_texto != ""): ?>
      <div class="alert alert-success"><?php echo $msg_texto; ?></div>
    <?php endif; ?>
    
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.3/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/js/bootstrap.min.js"></script>
</body>

</html>
